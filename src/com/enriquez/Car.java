package com.enriquez;

import javax.swing.plaf.synth.SynthTabbedPaneUI;

public class Car {
    // Class is a blueprint of a real-wold object.

    // Properties (Attributes)
        // Car --> Model Name, Brand Name, Manufacturing Year
    // Syntax
    // accessModifier dataType identifier;
    private String modelName;
    private String brandName;
    private int manufacturingYear;

    // Constructor
    // A special method  (function) that will enable us to setup the configuration of new object
    // 1) Empty Constructor
    // Syntax
    // accessModifier className(){...}
    public Car(){
        System.out.println("A new car object was created.");
    }

    // 2) Parameterized Constructor
    // Syntax
    // accessModifier className(dataType param, [dataType param, ...]){}
    public Car(String newBrandName, String newModelName, int newManufacturingYear){
        System.out.println("A new car object was created.");
        this.brandName = newBrandName;
        this.modelName = newModelName;
        this.manufacturingYear = newManufacturingYear;
    }

    // Methods (Behaviours) --> a function for a particular class
        // Car --> Drive, Stop, Describe
    // Syntax
    // accessModifier returnDataType  methodName(){...}

    // Getters --> retrieves values
    public String getBrandName() {
        return  this.brandName;
    }

    // Setters --> updates values
    public void setBrandName(String newBrandName){
        this.brandName = newBrandName;
    }

    // Mini-activity
    // Create a getter function for modelName and manufacturing Year properties
    public String getModelName() {
        return  this.modelName;
    }
    public int getManufacturingYear() {
        return  this.manufacturingYear;
    }

    // Create the setter function for modelName and manufacturingYear properties
    public void setModelName(String newModelName) {
        this.modelName = newModelName;
    }
    public void setManufacturingYear(Integer newManufacturingYear) {
        this.manufacturingYear = newManufacturingYear;
    }

    // Car methods --> drive, stop, describe
    // Mini-activity
    // Create the following drive, stop, describe methods.
    // drive
    // Display a message that  "the car is being driven"
    public void drive(){
       System.out.println("the car is being driven.");
    }

    // stop
    // Display a message that  "the car is not moving"
    public void stop(){
        System.out.println("the car is not moving.");
    }

    // describe
    // Return a string of text that display the model, brand and manufacturing year of the car instance.
    public String describe(){
        return  "Model: " + this.getBrandName() + ", Brands: " + this.getModelName() + ", Manufacturing Year: " + this.getManufacturingYear();
    }

}
