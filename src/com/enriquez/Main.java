package com.enriquez;

public class Main {

    public static void main(String[] args) {
	    // Classes and Objects
        System.out.println("** Classes and Objects **");

        // Instantiate (copy) an object without initialization of properties
        // Syntax
        // className Identifier = new className();
        Car a = new Car(); // New instance or (copy) of our Car class
        a.setBrandName("Honda");
        System.out.println(a.getBrandName());
        //System.out.println(a.modelName); This will not work because *.modelName is in private access

        Car b = new Car("Mitsubishi", "Adventure", 2020); // New instance or (copy) of our Car class
        System.out.println(b.getBrandName());

        // Mini-activity
        // Create a new instance of the Car class with the following values
        // Brand Name: Toyota
        // Model Name: Vios
        // Manufacturing Year: 2021
        // Note: Update the parameter constructor of the Car class

        Car c = new Car("Toyota", "Vios", 2021);
        System.out.println("Brand Name: " + c.getBrandName());
        System.out.println("Model Name: " + c.getModelName());
        System.out.println("Manufacturing Year: " + c.getManufacturingYear());

        Car d = new Car("Nissan", "Almera", 2020);
        System.out.println(d.describe());
        d.drive();
        d.stop();
    }
}
