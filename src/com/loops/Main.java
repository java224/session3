package com.loops;

import java.util.ArrayList;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        System.out.println("** Looping Statement **");
        // For Loops
        System.out.println("** For Loops **");
        for (int i = 0; i <= 5; i++){
            System.out.println(i);
        }

        // For Loops with Arrays
        System.out.println("** For Loops with Arrays **");
       int[] hundredList = {100, 200, 300, 400, 500 };
       for (int i = 0; i < hundredList.length; i++){
           System.out.println(hundredList[i]);
       }

        // ForEach Loops with Arrays
        System.out.println("** ForEach Loops with Arrays **");
        String[] nameList = {"John", "Paul", "George", "Ringo"};
        for (String name : nameList){
            System.out.println(name);
        }

        // ForEach Loops with ArrayList
        System.out.println("** ForEach Loops with ArrayList **");
        ArrayList<String> disneyCharacterList = new ArrayList<>(Arrays.asList("Mickey","Donald","Goofy"));
        for (String character : disneyCharacterList){
            System.out.println(character);
        }

        // Looping Through Multidimensional Arrays
        System.out.println("** Looping Through Multidimensional Arrays **");
        String[][] classroom = new String[3][3];

        //First row
        classroom[0][0] = "Athos";
        classroom[0][1] = "Porthos";
        classroom[0][2] = "Aramis";

        //Second row
        classroom[1][0] = "Mickey";
        classroom[1][1] = "Donald";
        classroom[1][2] = "Goofy";

        //Third row
        classroom[2][0] = "Harry";
        classroom[2][1] = "Ron";
        classroom[2][2] = "Hermione";

        for(int row = 0; row < 3; row++){
            for (int col = 0; col < 3; col++){
                System.out.println(classroom[row][col]);
            }
        }

        // While Loops
        System.out.println("** While Loops **");
        int x = 1;
        while (x < 5){
            System.out.println("Loop Number " + x);
            x++;
        }

        // Do-While Loops
        System.out.println("** Do-While Loops **");
        int y = 0;
        do {
            System.out.println("Countdown " + y);
            y--;
        } while (y > 10 );
    }
}
